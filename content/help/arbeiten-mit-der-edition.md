### Wie kann ich die digitale Edition benutzen?

<a href="https://www.youtube-nocookie.com/embed/iyHBtjJ9l7M?rel=0&start=17&end=38" target="_blank"><img src="/Resources/Public/Images/youtube-screenshot.png" width="480" /></a>

Über den Menüpunkt „Edition“ gelangen Sie zur Arbeitsoberfläche der Edition. Hier können Sie die
Vorlesungsmitschriften in verschiedenen Ansichten öffnen, in ihnen Suchen und über die
ausgezeichneten Personen, Orte, Literatur und Werke zu externen Datenbanken gelangen, die
weiterführende Informationen zu den jeweiligen Objekten oder die digitalisierte Literatur selbst
bereitstellen.
