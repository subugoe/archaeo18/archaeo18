###Wie kann ich mir die Arbeitsoberfläche individuell einrichten? Die manuelle Ausrichtung:

<a href="https://www.youtube-nocookie.com/embed/iyHBtjJ9l7M?rel=0&start=507&end=525" target="_blank"><img src="/Resources/Public/Images/youtube-screenshot.png" width="480" /></a>

Es sind beliebig viele Arbeitsmappen zur Dokumentanzeige
auf der Arbeitsfläche ablegbar. Die Mappen und der Browser können
beliebig auf der Arbeitsoberfläche angeordnet – meint verschoben,
vergrößert, verkleinert minimiert – werden.
