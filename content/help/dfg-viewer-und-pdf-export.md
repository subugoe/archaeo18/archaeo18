###DFG-Viewer und PDF Export

<a href="https://www.youtube-nocookie.com/embed/iyHBtjJ9l7M?rel=0&start=40&end=57" target="_blank"><img src="/Resources/Public/Images/youtube-screenshot.png" width="480" /></a>

Über die Navigation in der oberen rechten Ecke einer Mappe kann das
Digitalisat der Mitschrift im DFG-Viewer betrachtet werden. Zudem kann
die Transkription als PDF-Dokument heruntergeladen werden.
