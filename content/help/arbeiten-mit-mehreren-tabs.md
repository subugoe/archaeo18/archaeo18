### Wie kann ich dieVorlesungsmitschriften in Tabs ablegen: Die Mappenverwaltung

<a href="https://www.youtube-nocookie.com/embed/iyHBtjJ9l7M?rel=0&start=59&end=80" target="_blank"><img src="/Resources/Public/Images/youtube-screenshot.png" width="480" /></a>
Die Mitschriften können in einer neuen Mappe geöffnet oder in ein und derselben Mappe als mehrere Tabs abgelegt werden.
Haben Sie über den Browser mehrere Mitschriften oder Suchergebnisse in
einer Mappe geöffnet, können Sie durch Anwahl eines Tabs das entsprechende Dokument anzeigen lassen.
Ein Klick auf das Kreuz rechts neben dem Tab-Namen schließt den jeweiligen Tab.
