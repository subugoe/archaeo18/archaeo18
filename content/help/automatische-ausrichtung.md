### Wie kann ich die Arbeitsoberfläche optimal ausnutzen? Der Vollbildmodus und die automatische Ausrichtung:

<a href="https://www.youtube-nocookie.com/embed/iyHBtjJ9l7M?rel=0&start=474&end=505" target="_blank"><img src="/Resources/Public/Images/youtube-screenshot.png" width="480" /></a>

Mit der Aktivierung des Buttons „automatische Ausrichtung“ in der oberen
rechten Ecke der Arbeitsoberfläche können Sie alle Mappen nebeneinander
anordnen lassen. Einmal aktiviert, werden alle neuen Mappen automatisch
ausgerichtet. Der Modus kann jederzeit wieder deaktiviert werden. Mit
dem kleinen „Aufwärtspfeil“ ganz oben auf der Projektseite kann außerdem
das Menu mit dem Titel der Webseite und der Seitennavigation eingeklappt
werden, um so die Arbeitsfläche zu vergrößern. Um die Größe der
Arbeitsoberfläche noch weiter zu optimieren, besteht darüber hinaus die
Möglichkeit, den Vollbildmodus zu aktivieren (ebenfalls in der oberen
rechten Ecke des Arbeitsplatzes). Durch Betätigen der Taste „Escape“
(esc) auf Ihrer Computertastatur können Sie den Vollbildmodus jederzeit
wieder verlassen.
